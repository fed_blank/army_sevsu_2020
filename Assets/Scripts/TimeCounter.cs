﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeCounter : MonoBehaviour
{
    [SerializeField] private Text timeText;
    private float currentSec;

    private void Start()
    {
        currentSec = 0;
        timeText.text = "0";

        StartCoroutine(TimeAddeter());

    }

    private IEnumerator TimeAddeter()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);

            currentSec++;
            int min = (int)(currentSec / 60);
            int sec = (int)(currentSec % 60);
            timeText.text = min.ToString()+":"+ sec.ToString()+" сек";
        }
    }
}