﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : PageUI
{
    #region Unity Editor

    [Header("Buttons")]
    [SerializeField] private Button quitButton;
    [SerializeField] private Button breadBoardLocationButton;

    [Header("Sliders")]
    [SerializeField] private Slider volumeSlider;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        volumeSlider.onValueChanged.AddListener((value) =>
        {
            BackgrounMusicController.Instance.SetVolume(value);
        });

        quitButton.onClick.AddListener(() =>
        {
            SceneLoader.Instance.QuitApp();
        });

        breadBoardLocationButton.onClick.AddListener(() =>
        {
            SceneLoader.Instance.LoadNextScene();
        });
    }

    private void Start()
    {
        volumeSlider.value = BackgrounMusicController.Instance.GetVolume();
    }

    #endregion

}
