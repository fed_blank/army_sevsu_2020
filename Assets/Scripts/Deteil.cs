﻿using UnityEngine;

/// <summary>
/// Основной класс для каждой детали
/// </summary>
public class Deteil : MonoBehaviour
{
    #region Properties

    public SolderingIron.Types Type { get => type; set => type = value; }
    public float MinTemperature { get => minTemperature; set => minTemperature = value; }
    public float MaxTemperature { get => maxTemperature; set => maxTemperature = value; }

    #endregion

    #region Unity Editor

    [SerializeField] private SolderingIron.Types type;
    [SerializeField] private float minTemperature = 0;
    [SerializeField] private float maxTemperature = 1000;

    #endregion
}
