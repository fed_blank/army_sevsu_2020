﻿using UnityEngine;

/// <summary>
/// Базовый класс для страниц UI
/// </summary>
public class PageUI : MonoBehaviour
{

    #region Unity Editor

    [SerializeField] protected GameObject _currentPage;

    #endregion

    #region Public Methods

    /// <summary>
    /// Просмотр 
    /// </summary>
    virtual public void Show()
    {
        _currentPage.SetActive(true);
    }

    /// <summary>
    /// Закрытие
    /// </summary>
    public virtual void Hide()
    {
        _currentPage.SetActive(false);
    }

    #endregion

}
