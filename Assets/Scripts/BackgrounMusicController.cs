﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class BackgrounMusicController : MonoBehaviour
{
    #region Constants 

    private string MUSIC_VOLUME = "back_music";

    #endregion

    #region Properties

    public static BackgrounMusicController Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new GameObject("BackgrounMusicController").AddComponent<BackgrounMusicController>();
            }
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    #endregion

    #region Private Field

    private static BackgrounMusicController instance = null;
    private AudioSource audioSource;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
            return;
        }
    }

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        if (PlayerPrefs.HasKey(MUSIC_VOLUME))
        {
            audioSource.volume = PlayerPrefs.GetFloat(MUSIC_VOLUME);
        }
    }

    #endregion

    #region PublicMethods

    /// <summary>
    /// Установка громкости фоновой музыки
    /// </summary>
    /// <param name="value">значение громкости</param>
    public void SetVolume(float value)
    {
        audioSource.volume = value;
        PlayerPrefs.SetFloat(MUSIC_VOLUME, value);  //не очищать
        PlayerPrefs.Save();
    }

    /// <summary>
    /// Получение значения громкости музыки
    /// </summary>
    /// <returns></returns>
    public float GetVolume()
    {
        return audioSource.volume;
    }

    #endregion
}
