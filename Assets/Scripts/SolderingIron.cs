﻿using System;
using UnityEngine;

/// <summary>
/// Основной класс паяльника
/// </summary>
public class SolderingIron : MonoBehaviour
{
    #region Unity Editor
    public enum Types
    {
        softSolder,
        hardSolder
    }

    [SerializeField] public static Types type;
    [Range(0,500)] public static float temperature = 0;
    #endregion

    #region Public Methods

    #endregion
}
