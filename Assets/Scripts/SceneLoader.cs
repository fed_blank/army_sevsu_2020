﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Работа со сценами - загрузка, выход
/// </summary>
public class SceneLoader : MonoBehaviour
{

    #region Constants

    private const int START_SCENE_INDEX = 0;

    #endregion



    #region Properties

    public static SceneLoader Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new GameObject("SceneLoader").AddComponent<SceneLoader>();
            }
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    #endregion

    #region Private Fields

    private static SceneLoader instance = null;

    #endregion

    #region UnityMethods

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Загрузка стартовой сцены
    /// </summary>
    public void LoadStartScene()
    {
        SceneManager.LoadScene(START_SCENE_INDEX);
    }

    /// <summary>
    /// Загрузка следующей сцены по индексу
    /// </summary>
    public void LoadNextScene()
    {
        int currentScene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentScene + 1);
    }

    /// <summary>
    /// Выход из приложения
    /// </summary>
    public void QuitApp()
    {
        Debug.Log("quit app");
        Application.Quit();
    }

    #endregion

}
